package com.example.quan_ly_cham_cong.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;

import com.example.quan_ly_cham_cong.R;
import com.example.quan_ly_cham_cong.fragment.Fragmen_dscc;
import com.example.quan_ly_cham_cong.fragment.Fragmen_qlchamcong;
import com.example.quan_ly_cham_cong.fragment.Fragment_Them;
import com.example.quan_ly_cham_cong.fragment.Fragment_dssp;
;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    AppCompatButton btn_ds, btn_bttcc, btn_ds_sp;
    ConstraintLayout constrain_main;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        anhXa();
        eventClick();
    }

    private void anhXa(){
        btn_ds = this.findViewById(R.id.btn_ds_cn);
        constrain_main = this.findViewById(R.id.constrain_main);
        btn_bttcc = this.findViewById(R.id.btn_bttcc);
        btn_ds_sp = this.findViewById(R.id.btn_ds_sp);
    }

    private void eventClick(){
        btn_ds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().beginTransaction()
                        .setReorderingAllowed(true)
                        .add(R.id.fragment_container_view, Fragment_Them.class, null)
                        .commit();
                constrain_main.setVisibility(View.GONE);
            }
        });

        btn_bttcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().beginTransaction()
                        .setReorderingAllowed(true)
                        .add(R.id.fragment_container_view, Fragmen_dscc.class, null)
                        .commit();
                constrain_main.setVisibility(View.GONE);
            }
        });

        btn_ds_sp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().beginTransaction()
                        .setReorderingAllowed(true)
                        .add(R.id.fragment_container_view, Fragment_dssp.class, null)
                        .commit();
                constrain_main.setVisibility(View.GONE);
            }
        });

    }

}