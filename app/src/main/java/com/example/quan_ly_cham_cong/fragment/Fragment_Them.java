package com.example.quan_ly_cham_cong.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;

import com.example.quan_ly_cham_cong.R;
import com.example.quan_ly_cham_cong.database.AppDatabase;
import com.example.quan_ly_cham_cong.model.ChamCong;
import com.example.quan_ly_cham_cong.model.CongNhan;

public class Fragment_Them extends Fragment {
    View view;
    TextView nameCongNhan, maCongNhan, phanXuong;
    AppCompatButton button_luu, btn_huy;
    AppDatabase database;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        database = AppDatabase.getDatabaseInstance(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragmen_themcn,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        nameCongNhan = view.findViewById(R.id.tv_4);
        maCongNhan = view.findViewById(R.id.tv_5);
        phanXuong = view.findViewById(R.id.tv_6);
        button_luu = view.findViewById(R.id.luu);
        btn_huy = view.findViewById(R.id.huy);
        insertDatabase();

    }

    private void insertDatabase(){
        button_luu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CongNhan congNhan = new CongNhan();
                congNhan.setMaCN(maCongNhan.getText().toString());
                congNhan.setTenCN(maCongNhan.getText().toString());
                congNhan.setPhanXuong(maCongNhan.getText().toString());
                database.congNhanDAO().insert(congNhan);
                Log.d("toanpk", String.valueOf(database.congNhanDAO().getListCongNhan().size()));
            }
        });

//        btn_huy.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                database.congNhanDAO().delete(nameCongNhan.getText().toString());
//                Log.d("delete", String.valueOf(database.congNhanDAO().getListCongNhan().size()));
//            }
//        });
    }


}
