package com.example.quan_ly_cham_cong.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;

import com.example.quan_ly_cham_cong.R;
import com.example.quan_ly_cham_cong.database.AppDatabase;
import com.example.quan_ly_cham_cong.model.SanPham;

public class Fragment_edit_sp extends Fragment{
    View view;
    EditText tv_masp, tv_tensp2,dongia;
    AppCompatButton btn_luu;
    AppDatabase database;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        database = AppDatabase.getDatabaseInstance(getContext());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        tv_masp = view.findViewById(R.id.tv_masp2);
        tv_tensp2 = view.findViewById(R.id.tv_tensp2);
        dongia = view.findViewById(R.id.dongia);
        btn_luu = view.findViewById(R.id.luu);

        updateSP();
    }
    private void updateSP(){
        btn_luu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SanPham sanPham = new SanPham();
                sanPham.setTenSP(tv_tensp2.getText().toString());
                sanPham.setDonGia(Float.parseFloat(dongia.getText().toString()));
                sanPham.setMaSP(tv_masp.getText().toString());
                database.sanPhamDAO().updateSP(sanPham);
            }
        });
    }
}


