package com.example.quan_ly_cham_cong.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quan_ly_cham_cong.R;
import com.example.quan_ly_cham_cong.adapter.DsccAdapter;
import com.example.quan_ly_cham_cong.database.AppDatabase;
import com.example.quan_ly_cham_cong.model.ChamCong;

import java.util.ArrayList;

public class Fragmen_dscc extends Fragment {

    private DsccAdapter adapter;
    private ArrayList<ChamCong> listCC = new ArrayList<>();
    RecyclerView rcv_ds_cc;
    View view;
    AppCompatButton btn_add;
    EditText edt_ma_cc;
    EditText edt_ngay_cc;
    EditText edt_ma_cn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new DsccAdapter();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view= inflater.inflate(R.layout.fragment_ds_cc,container,false);

         return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        btn_add = view.findViewById(R.id.themcn);
        edt_ma_cc = view.findViewById(R.id.edt_ma_cc);
        edt_ngay_cc = view.findViewById(R.id.edt_ngay_cc);
        edt_ma_cn = view.findViewById(R.id.edt_ma_cn);
        rcv_ds_cc = view.findViewById(R.id.rcv_dssp);
        addData();
    }

    private void addData(){
        listCC.add(new ChamCong(0, "CC01", "14/09/2018", "CN01", null));
        listCC.add(new ChamCong(1, "CC02", "14/09/2018", "CN02", null));
        listCC.add(new ChamCong(2, "CC03", "14/09/2018", "CN03", null));
        listCC.add(new ChamCong(0, "CC04", "25/09/2018", "CN04", null));
        adapter.addData(listCC);
        rcv_ds_cc.setAdapter(adapter);
        rcv_ds_cc.setLayoutManager(new LinearLayoutManager(getContext()));
    }
}
