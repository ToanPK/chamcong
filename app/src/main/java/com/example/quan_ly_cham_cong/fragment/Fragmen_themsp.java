package com.example.quan_ly_cham_cong.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;

import com.example.quan_ly_cham_cong.R;
import com.example.quan_ly_cham_cong.database.AppDatabase;
import com.example.quan_ly_cham_cong.database.SanPhamDAO;
import com.example.quan_ly_cham_cong.model.SanPham;

public class Fragmen_themsp extends Fragment {
    View view;
    EditText tv_ten2, tv_sp3, ten_masp, masp;
    AppCompatButton btn_luu;
    AppDatabase database;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        database = AppDatabase.getDatabaseInstance(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragmen_themsp,container,false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        tv_ten2 = view.findViewById(R.id.tv_ten2);
        tv_sp3 = view.findViewById(R.id.tv_sp3);
        btn_luu = view.findViewById(R.id.luu);
        masp = view.findViewById(R.id.ten_masp);
        queryDatabase();
    }

    private void queryDatabase(){
        btn_luu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SanPham sanPham = new SanPham();
                sanPham.setMaSP(masp.getText().toString());
                sanPham.setDonGia(Float.parseFloat(tv_sp3.getText().toString()));
                sanPham.setTenSP(tv_ten2.getText().toString());
                database.sanPhamDAO().insertSanPham(sanPham);
                Log.d("toanpk_dssp", String.valueOf(database.sanPhamDAO().getListSP().size()));
            }
        });
    }



}
