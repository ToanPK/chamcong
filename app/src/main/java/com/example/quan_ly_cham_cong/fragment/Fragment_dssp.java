package com.example.quan_ly_cham_cong.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quan_ly_cham_cong.R;
import com.example.quan_ly_cham_cong.adapter.DsspAdapter;
import com.example.quan_ly_cham_cong.database.AppDatabase;
import com.example.quan_ly_cham_cong.model.SanPham;

import java.util.ArrayList;

public class Fragment_dssp extends Fragment {
    View view;
    private DsspAdapter dsspAdapter;
    RecyclerView rcv_dssp;
    AppCompatButton them_sp;
    AppDatabase database;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dsspAdapter = new DsspAdapter();
        database = AppDatabase.getDatabaseInstance(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragmen_ds_sp,container,false) ;

        return view;
    }
    @Nullable
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        dsspAdapter.addData((ArrayList<SanPham>) database.sanPhamDAO().getListSP());
        rcv_dssp = view.findViewById(R.id.rcv_dssp);
        rcv_dssp.setAdapter(dsspAdapter);
        rcv_dssp.setLayoutManager(new LinearLayoutManager(getContext()));
        them_sp = view.findViewById(R.id.themsp);
        them_sp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container_view, Fragmen_themsp.class, null, "tag")
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

}
