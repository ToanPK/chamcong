package com.example.quan_ly_cham_cong.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.quan_ly_cham_cong.model.ChamCong;

import java.util.List;

@Dao
public interface ChamCongDAO {
    @Insert
    public void insertChamCong(ChamCong chamCong);

    @Query("SELECT * FROM CHAMCONG")
    public List<ChamCong> listChamCong();


}
