package com.example.quan_ly_cham_cong.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.quan_ly_cham_cong.model.ChamCong;
import com.example.quan_ly_cham_cong.model.CongNhan;

import java.util.List;

//Annotation này là nơi mình định nghĩa các câu lệnh truy vấn cơ sở dữ liệu
@Dao
public interface CongNhanDAO {
    @Insert
    public void insert(CongNhan congNhan);

    @Query("SELECT * FROM congNhan")
    public List<CongNhan> getListCongNhan();

    @Query("DELETE FROM CongNhan WHERE TenCN=:name")
    public void delete(String name);

    @Update
    public void updateCongNhan(CongNhan congNhan);

}
