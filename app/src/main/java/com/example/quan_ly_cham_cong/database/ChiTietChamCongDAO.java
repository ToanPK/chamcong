package com.example.quan_ly_cham_cong.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.quan_ly_cham_cong.model.ChiTietChamCong;

import java.util.List;

@Dao
public interface ChiTietChamCongDAO {
    @Insert
    public void insert(ChiTietChamCong chiTietChamCong);

    @Query("SELECT * FROM ChiTietChamCong")
    public List<ChiTietChamCong> getListChiTietChamCong();
}
