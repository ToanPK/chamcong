package com.example.quan_ly_cham_cong.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.quan_ly_cham_cong.model.SanPham;

import java.util.List;

@Dao
public interface SanPhamDAO {
    @Insert
    public void insertSanPham(SanPham sanPham);

    @Query("SELECT * FROM SanPham")
    public List<SanPham> getListSP();

    @Update
    public void updateSP(SanPham sanPham);
}
