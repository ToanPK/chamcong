package com.example.quan_ly_cham_cong.model;




import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.List;


@Entity
public class CongNhan {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String maCN;
    private String HoCN;
    private String TenCN;
    private String PhanXuong;
    private List<ChamCong> listChamCong;

    public CongNhan(int id, String maCN, String hoCN, String tenCN, String phanXuong, List<ChamCong> listChamCong) {
        this.id = 0;
        this.maCN = maCN;
        HoCN = hoCN;
        TenCN = tenCN;
        PhanXuong = phanXuong;
        this.listChamCong = listChamCong;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaCN() {
        return maCN;
    }

    public void setMaCN(String maCN) {
        this.maCN = maCN;
    }

    public String getHoCN() {
        return HoCN;
    }

    public void setHoCN(String hoCN) {
        HoCN = hoCN;
    }

    public String getTenCN() {
        return TenCN;
    }

    public void setTenCN(String tenCN) {
        TenCN = tenCN;
    }

    public String getPhanXuong() {
        return PhanXuong;
    }

    public void setPhanXuong(String phanXuong) {
        PhanXuong = phanXuong;
    }

    public List<ChamCong> getListChamCong() {
        return listChamCong;
    }

    public void setListChamCong(List<ChamCong> listChamCong) {
        this.listChamCong = listChamCong;
    }

    public CongNhan(){}
}
