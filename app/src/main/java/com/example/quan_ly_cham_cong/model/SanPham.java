package com.example.quan_ly_cham_cong.model;




import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity
public class SanPham {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String maSP;
    private String tenSP;
    private float donGia;

    public String getMaSP() {
        return maSP;
    }

    public SanPham(int id, String maSP, String tenSP, float donGia) {
        this.id = 0;
        this.maSP = maSP;
        this.tenSP = tenSP;
        this.donGia = donGia;
    }
    public SanPham(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMaSP(String maSP) {
        this.maSP = maSP;
    }

    public String getTenSP() {
        return tenSP;
    }

    public void setTenSP(String tenSP) {
        this.tenSP = tenSP;
    }

    public float getDonGia() {
        return donGia;
    }

    public void setDonGia(float donGia) {
        this.donGia = donGia;
    }

}
