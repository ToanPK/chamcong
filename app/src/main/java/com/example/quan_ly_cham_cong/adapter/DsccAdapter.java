package com.example.quan_ly_cham_cong.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quan_ly_cham_cong.R;
import com.example.quan_ly_cham_cong.model.ChamCong;

import java.util.ArrayList;

public class DsccAdapter extends RecyclerView.Adapter<ViewHolderDSCC> {
    private ArrayList<ChamCong> listCC = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolderDSCC onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolderDSCC(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cc, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDSCC holder, int position) {
        holder.bindData(listCC.get(position));
    }

    @Override
    public int getItemCount() {
        return listCC.size();
    }

    public void addData(ArrayList<ChamCong> listChamCong){
        this.listCC = listChamCong;
    }
}


class ViewHolderDSCC extends RecyclerView.ViewHolder{
    public ViewHolderDSCC(View view){
        super(view);
    }
    TextView tv_ma_cc;
    TextView tv_ngay_cc;
    TextView tv_ma_cn;

    void bindData(ChamCong chamCong){
        tv_ma_cc = itemView.findViewById(R.id.tv_ma_cc);
        tv_ngay_cc = itemView.findViewById(R.id.tv_ngay_cc);
        tv_ma_cn = itemView.findViewById(R.id.tv_ma_cn);

        tv_ma_cc.setText(chamCong.getMaCC());
        tv_ngay_cc.setText(chamCong.getNgayCC());
        tv_ma_cn.setText(chamCong.getMaCN());

    }
}
