package com.example.quan_ly_cham_cong.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quan_ly_cham_cong.R;
import com.example.quan_ly_cham_cong.model.SanPham;

import java.util.ArrayList;

public class DsspAdapter extends RecyclerView.Adapter<ViewHolderDssp> {
    private ArrayList<SanPham> listSp = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolderDssp onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolderDssp(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_ds_sp, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDssp holder, int position) {
        holder.bindData(listSp.get(position));
    }

    @Override
    public int getItemCount() {
        return listSp.size();
    }

    public void addData(ArrayList<SanPham> listSp){
        this.listSp = listSp;
    }
}


class ViewHolderDssp extends RecyclerView.ViewHolder{
    public ViewHolderDssp(View view){
        super(view);
    }
    TextView tv_ma;
    TextView tv_5;
    TextView tv_6;

    void bindData(SanPham sanPham){
        tv_ma = itemView.findViewById(R.id.tv_ma);
        tv_5 = itemView.findViewById(R.id.tv_5);
        tv_6 = itemView.findViewById(R.id.tv_6);

        tv_ma.setText(sanPham.getMaSP());
        tv_5.setText(sanPham.getTenSP());
        tv_6.setText(String.valueOf(sanPham.getDonGia()));

    }
}

